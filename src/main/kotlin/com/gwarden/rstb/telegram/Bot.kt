package com.gwarden.rstb.telegram

import com.gwarden.rstb.core.Respondent
import com.gwarden.rstb.utils.Config
import org.telegram.telegrambots.api.methods.send.SendMessage
import org.telegram.telegrambots.api.objects.Update
import org.telegram.telegrambots.bots.TelegramLongPollingBot

class Bot: TelegramLongPollingBot() {

    lateinit var tokens: Config
    lateinit var respondent: Respondent

    override fun getBotToken(): String? {
        return tokens.get("telegram")
    }

    override fun getBotUsername(): String {
        return "RstBot"
    }

    override fun onUpdateReceived(update: Update) {
        if (update.hasMessage()) {
            val query = update.message.text
            val response = respondent.respond(query)
            execute(SendMessage(update.message.chatId, response))
        }
    }
}