package com.gwarden.rstb.utils

fun <T> T.str() = this.toString()

fun <T> T.std(): T {
    println(this.str())
    return this
}