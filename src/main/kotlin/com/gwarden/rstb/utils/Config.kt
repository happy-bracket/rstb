package com.gwarden.rstb.utils

interface Config {

    fun get(key: String): String

    fun readFrom(path: String)

}