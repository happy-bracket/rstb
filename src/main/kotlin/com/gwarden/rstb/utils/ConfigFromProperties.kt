package com.gwarden.rstb.utils

import org.springframework.stereotype.Component
import java.io.File
import javax.annotation.PostConstruct

@Component
class ConfigFromProperties: Config {

    var storage: Map<String, String> = emptyMap()

    @PostConstruct
    fun defaultInit() {
        readFrom("/private/tokens.properties")
    }

    override fun get(key: String) = storage[key].orEmpty()

    override fun readFrom(path: String) {
        storage = this.javaClass.getResource(path).let { File(it.toURI()) }.readLines().map { it.split("=") }.map { it[0] to it[1] }.toMap()
    }
}