package com.gwarden.rstb.datalayer

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface QuestionsRepository: CrudRepository<Question, Int>