package com.gwarden.rstb.datalayer

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
data class Answer(
       @Id @GeneratedValue var id: Int = 0,
       var body: String = ""
)