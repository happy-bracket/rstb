package com.gwarden.rstb.datalayer

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
data class Question(
     @Id @GeneratedValue var id: Int = 0,
     var header: String = "",
     var answer: Int = 0
)