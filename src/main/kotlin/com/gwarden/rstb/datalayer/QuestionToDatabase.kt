package com.gwarden.rstb.datalayer


interface QuestionToDatabase {

    fun addQuestion(headers: String, body: String, delimiter: String)

}