package com.gwarden.rstb

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class RstbApplication

fun main(args: Array<String>) {
    SpringApplication.run(RstbApplication::class.java, *args)
}
