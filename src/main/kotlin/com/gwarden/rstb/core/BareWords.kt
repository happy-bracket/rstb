package com.gwarden.rstb.core

import org.springframework.stereotype.Component

@Component
class BareWords: SuitabilityChecker {

    override fun similarity(one: String, other: String): Float {
        val wordsOfOne = one.split(Regex("[ ?,.]")).map { it.toLowerCase() }
        val amountOfWordsOfOne = wordsOfOne.size
        val similarity = other.split(" ").map { it.toLowerCase() }.map { it.replace(Regex("[!?,.]"), "") }.map { (if (wordsOfOne.contains(it)) 1 else 0) }.sum().toFloat() / amountOfWordsOfOne.toFloat()
                return similarity
    }

    override fun suitability(coefficient: Float): Boolean = coefficient > 0.25

}