package com.gwarden.rstb.core


class ErrorOracle {

    companion object {

        fun emptyMessage() = "Попробуйте ввести запрос ещё раз"

        fun noSuitable() = "К сожалению, я пока не могу ответить на ваш вопрос. Попробуйте воспользоваться командами!"

    }

}