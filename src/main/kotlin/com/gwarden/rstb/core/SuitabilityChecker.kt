package com.gwarden.rstb.core

interface SuitabilityChecker {

    fun similarity(one: String, other: String): Float

    fun suitability(coefficient: Float): Boolean

}