package com.gwarden.rstb.core

import com.gwarden.rstb.datalayer.AnswersRepository
import com.gwarden.rstb.datalayer.QuestionsRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class DirectRespondent(
        @Autowired val questions: QuestionsRepository,
        @Autowired val answers: AnswersRepository,
        @Autowired val check: SuitabilityChecker
): Respondent {

    override fun respond(question: String): String =
            questions
                    .findAll()
                    .map { it to check.similarity(question, it.header) }
                    .filter { check.suitability(it.second) }
                    .run {
                        if (isEmpty())
                            ErrorOracle.noSuitable()
                        else
                            answers.findOne(maxBy { it.second }!!.first.answer).body
                    }

}