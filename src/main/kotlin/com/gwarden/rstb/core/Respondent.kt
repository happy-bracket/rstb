package com.gwarden.rstb.core

interface Respondent {

    fun respond(question: String): String

}