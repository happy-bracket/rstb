package com.gwarden.rstb.messengers

import com.gwarden.rstb.utils.std
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.stereotype.Controller
import javax.annotation.PostConstruct

@Controller
@ConditionalOnProperty(name = arrayOf("bots.enabled"), matchIfMissing = true)
class MessengersHolder(
        @Autowired() @Qualifier("telegram") val telegram: Messenger?
) {

    @PostConstruct
    fun start() {
        telegram?.launch()
    }

}