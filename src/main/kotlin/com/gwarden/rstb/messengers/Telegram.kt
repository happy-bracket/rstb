package com.gwarden.rstb.messengers

import com.gwarden.rstb.core.Respondent
import com.gwarden.rstb.telegram.Bot
import com.gwarden.rstb.utils.Config
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.stereotype.Component
import org.telegram.telegrambots.ApiContextInitializer
import org.telegram.telegrambots.TelegramBotsApi

@Component("telegram")
@ConditionalOnProperty(name = arrayOf("bots.telegram.enabled"), matchIfMissing = true)
class Telegram(
        @Autowired val config: Config,
        @Autowired val respondent: Respondent
): Messenger {

    private var bot: Bot? = null

    override fun launch() {
        ApiContextInitializer.init()
        val botsApi = TelegramBotsApi()

        bot = Bot()

        bot?.tokens = config
        bot?.respondent = respondent

        botsApi.registerBot(bot)
    }

}