package com.gwarden.rstb.messengers

interface Messenger {

    fun launch()

}