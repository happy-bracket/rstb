package com.gwarden.rstb.admin

import com.gwarden.rstb.datalayer.AnswersRepository
import com.gwarden.rstb.datalayer.QuestionToDatabase
import com.gwarden.rstb.datalayer.QuestionsRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/admin")
@ConditionalOnProperty(name = arrayOf("admin.enabled"), matchIfMissing = true)
class AdminController(
        @Autowired val questionsAppender: QuestionToDatabase?,
        @Autowired val questionsRepo: QuestionsRepository,
        @Autowired val responsesRepo: AnswersRepository
) {

    @PostMapping("/add_question")
    fun addQuestion(
            @RequestParam("headers") headers: String,
            @RequestParam("content") answer: String,
            @RequestParam(value = "delimiter", defaultValue = ";") delimiter: String
    ) {
        questionsAppender?.addQuestion(headers, answer, delimiter)
    }

    @PostMapping("/get_questions")
    fun getQuestions() = questionsRepo.findAll().map { it.header to it.answer }

    @PostMapping("/get_response")
    fun getResponse(@RequestParam(name = "id", required = true) responseId: Int) = responsesRepo.findOne(responseId)

    @PostMapping("/get_responses")
    fun getResponses(
            @RequestParam(defaultValue = "-1") responseId: Int = -1
    ) = responsesRepo.findAll().map { it.id to it.body }

    @PostMapping("/get_all")
    fun getAll(): Map<*, *> {
        val questions = questionsRepo.findAll()
        val responses = responsesRepo.findAll().map {
            val id = it.id
            mapOf(
                    "body" to it.body,
                    "questions" to questions.filter { it.answer == id }.map { it.header }
            ) }
        val model = mapOf(
                "responses" to responses
        )
        return model
    }
}
