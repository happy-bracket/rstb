package com.gwarden.rstb.admin

import com.gwarden.rstb.datalayer.AnswersRepository
import com.gwarden.rstb.datalayer.QuestionsRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.servlet.ModelAndView

@Controller
class FrontendController(
        @Autowired val questions: QuestionsRepository,
        @Autowired val responses: AnswersRepository
) {

    @GetMapping("/")
    fun index(): ModelAndView {
        val questions = questions.findAll()
        val responses = responses.findAll().map {
            val id = it.id
            mapOf(
                "body" to id,
                "questions" to questions.filter { it.answer == id }.map { it.header }
        ) }
        val model = mapOf(
                "responses" to responses
        )
        return ModelAndView("index", model)
    }

    @GetMapping("/login")
    fun login() = ModelAndView("login")

}