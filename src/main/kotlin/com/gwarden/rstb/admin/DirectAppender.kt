package com.gwarden.rstb.admin

import com.gwarden.rstb.core.SuitabilityChecker
import com.gwarden.rstb.datalayer.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class DirectAppender(
        @Autowired val questions: QuestionsRepository,
        @Autowired val answers: AnswersRepository,
        @Autowired val calculate: SuitabilityChecker
): QuestionToDatabase {

    override fun addQuestion(headers: String, body: String, delimiter: String) {
        val answer = findOrAddAnswer(body)
        headers.split(delimiter).map { Question(header = it, answer = answer.id) }.forEach { questions.save(it) }
    }

    private fun findOrAddAnswer(answer: String) =
            answers
                .findAll()
                .map { it to calculate.similarity(it.body, answer) }
                .filter { calculate.suitability(it.second) }
                .run {
                    if (isNotEmpty())
                        this.maxBy { it.second }!!.first
                    else
                        answers.save(Answer(body = answer.toLowerCase()))
                }

}