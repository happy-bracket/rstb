package com.gwarden.rstb

import com.gwarden.rstb.core.BareWords
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class BareWordsSimilarityCheckTest {

    lateinit var check: BareWords

    private val e = 0.001f

    @Before
    fun setUp() {
        check = BareWords()
    }

    @Test
    fun testSimilarityCheck() {
        var similarity: Float
        similarity = check.similarity("I was the sun", "Before it was cool")
        assertEquals(.25f, similarity, e)
        similarity = check.similarity("I was the sun", "I was the moon")
        assertEquals(.75f, similarity, e)
        similarity = check.similarity("i was the sun", "I was the sun")
        assertEquals(1.0f, similarity, e)
    }

}