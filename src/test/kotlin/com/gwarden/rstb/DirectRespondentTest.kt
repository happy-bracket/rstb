package com.gwarden.rstb

import com.gwarden.rstb.datalayer.Answer
import com.gwarden.rstb.datalayer.AnswersRepository
import com.gwarden.rstb.datalayer.Question
import com.gwarden.rstb.datalayer.QuestionsRepository
import com.gwarden.rstb.core.BareWords
import com.gwarden.rstb.core.DirectRespondent
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

class DirectRespondentTest {

    lateinit var respondent: DirectRespondent

    @Before
    fun setUp() {
        respondent = DirectRespondent(
                TestQuestionsRepositoryImpl(),
                TestAnswersRepositoryImpl(),
                BareWords()
        )
    }

    @Test
    fun testResponse() {
        var question: String
        var response: String

        question = "Who invaded into our garden?"
        response = respondent.respond(question)
        assertEquals("En garde!", response)

        question = "Where is Fernand?"
        response = respondent.respond(question)
        assertEquals("Oh, Dantes, do you still love me?", response)
    }

}

class TestQuestionsRepositoryImpl: QuestionsRepository {

    override fun findOne(id: Int?): Question = null!!

    override fun exists(id: Int?): Boolean = null!!

    override fun count(): Long = null!!

    override fun findAll(ids: MutableIterable<Int>?): MutableIterable<Question> = null!!

    override fun findAll(): MutableIterable<Question> = mutableListOf(
            Question(0, "Who trespassed our garden?", 3),
            Question(1, "Was I the sun?", 0),
            Question(2, "Fernand, the traitor! Where is he?", 2),
            Question(3, "Was fuse ignited?", 1),
            Question(4, "Mercedes, am I back in Marseilles?", 2),
            Question(5, "I was doing all the things that will be cool?", 0)
    )

    override fun delete(entities: MutableIterable<Question>?) {
    }

    override fun delete(entity: Question?) {
    }

    override fun delete(id: Int?) {
    }

    override fun <S : Question?> save(entity: S): S = null!!

    override fun <S : Question?> save(entities: MutableIterable<S>?): MutableIterable<S> = null!!

    override fun deleteAll() {
    }

}

class TestAnswersRepositoryImpl: AnswersRepository {

    private val answers = mutableListOf(
            Answer(0, "Before it was cool."),
            Answer(1, "And they were gone!"),
            Answer(2, "Oh, Dantes, do you still love me?"),
            Answer(3, "En garde!")
    )

    override fun findOne(id: Int?): Answer = answers.find { it.id == id }!!

    override fun delete(entities: MutableIterable<Answer>?) {
    }

    override fun delete(id: Int?) {
    }

    override fun delete(entity: Answer?) {
    }

    override fun findAll(ids: MutableIterable<Int>?): MutableIterable<Answer> = null!!

    override fun findAll(): MutableIterable<Answer> = answers

    override fun exists(id: Int?): Boolean = null!!

    override fun <S : Answer?> save(entity: S): S = null!!

    override fun <S : Answer?> save(entities: MutableIterable<S>?): MutableIterable<S> = null!!

    override fun deleteAll() {
    }

    override fun count(): Long = null!!

}