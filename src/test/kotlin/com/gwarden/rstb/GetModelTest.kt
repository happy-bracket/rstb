package com.gwarden.rstb

import com.gwarden.rstb.admin.AdminController
import com.gwarden.rstb.admin.DirectAppender
import com.gwarden.rstb.utils.std
import org.json.JSONObject
import org.junit.Test

class GetModelTest {

    var controller: AdminController = AdminController(
            null,
            TestQuestionsRepositoryImpl(),
            TestAnswersRepositoryImpl()
    )

    @Test
    fun getModel() {
        JSONObject(controller.getAll()).toString().std()
    }

}